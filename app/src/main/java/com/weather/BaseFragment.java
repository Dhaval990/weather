package com.weather;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.weather.interfaces.ActImpMethods;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;


/**
 * Created by Dhaval Joshi on 15/11/16.
 */

public abstract class BaseFragment extends Fragment implements ActImpMethods {
    public static final String TAG = BaseFragment.class.getSimpleName();
    protected ViewDataBinding binding;
    protected int layoutId;
    boolean hasOptionMenu = false;


    protected void setView(int layoutId) {
        this.layoutId = layoutId;
        hasOptionMenu = false;

    }

    protected void setView(int layoutId, boolean hasOptionMenu) {
        this.layoutId = layoutId;
        this.hasOptionMenu = hasOptionMenu;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, layoutId, container, false);
        setHasOptionsMenu(hasOptionMenu);
        return binding.getRoot();
    }

    protected <T extends ViewDataBinding> T getBinding() {
        return (T) binding;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            initVariable();
            loadData();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    protected void setToolbarTitle(String title) {
        ((BaseActivity) getContext()).setToolBarTitle(title);
    }


}
