package com.weather.util;


import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.android.material.snackbar.Snackbar;
import com.weather.Constants;
import com.weather.R;
import com.weather.databinding.DialogOkCancelBinding;
import com.weather.fragment.CustomDialogDataBind;
import com.weather.interfaces.DialogBookmarkCallback;
import com.weather.interfaces.DialogCallback;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;

import static androidx.vectordrawable.graphics.drawable.VectorDrawableCompat.create;
import static com.weather.Constants.UNIT_SYSTEM;

public class Helper {

    public static void showOKCancelDialog(Context context, String message, final DialogCallback callBack) {
        final DialogOkCancelBinding mBinding = DataBindingUtil.inflate(((Activity) context).getLayoutInflater(), R.layout.dialog_ok_cancel, null, false);
        final CustomDialogDataBind customDialogDataBind = new CustomDialogDataBind();
        android.content.DialogInterface.OnClickListener okListener = (dialogInterface, i) -> {
        };
        customDialogDataBind.initializeVariable(null, null, null, null, okListener, null, mBinding.getRoot());
        customDialogDataBind.show(((FragmentActivity) context).getSupportFragmentManager(), "Dialog");
        mBinding.txtMessage.setText(message);
        mBinding.btnOk.setOnClickListener(v ->
        {
            callBack.onDismiss(true);
            customDialogDataBind.dismiss();
        });
        mBinding.btnCancel.setOnClickListener(view -> {
            customDialogDataBind.dismiss();
            callBack.onDismiss(false);
        });
        customDialogDataBind.setCancelable(false);
    }

    public static void showUnitSystemDialog(Context context, final DialogBookmarkCallback callBack) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_unit_system, null);

        final CustomDialogDataBind customDialogDataBind = new CustomDialogDataBind();
        android.content.DialogInterface.OnClickListener okListener = (dialogInterface, i) -> {
        };
        customDialogDataBind.initializeVariable(null, null, null, null, okListener, null, view);
        Button btnOk = view.findViewById(R.id.btnOk);
        Button btnCancel = view.findViewById(R.id.btnCancel);


        RadioGroup rbgUnit = view.findViewById(R.id.rbg_unit);
        RadioButton rbMetric = view.findViewById(R.id.rbt_metric);
        RadioButton btnImperial = view.findViewById(R.id.rbt_imperial);

        customDialogDataBind.show(((FragmentActivity) context).getSupportFragmentManager(), "Dialog");

        if (PrefManager.getSharedPref(context, UNIT_SYSTEM, "").equalsIgnoreCase(Constants.METRIC)) {
            rbMetric.setChecked(true);
        } else if (PrefManager.getSharedPref(context, UNIT_SYSTEM, "").equalsIgnoreCase(Constants.IMPERIAL)) {
            btnImperial.setChecked(true);
        }


        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (rbgUnit.getCheckedRadioButtonId() == R.id.rbt_metric) {
                    PrefManager.setSharedPref(context, UNIT_SYSTEM, Constants.METRIC);
                    callBack.onDismiss(Constants.METRIC, true);
                } else {
                    callBack.onDismiss(Constants.IMPERIAL, true);
                    PrefManager.setSharedPref(context, UNIT_SYSTEM, Constants.IMPERIAL);
                }
                customDialogDataBind.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialogDataBind.dismiss();
                callBack.onDismiss(null, false);
            }
        });

        customDialogDataBind.setCancelable(false);
    }

    public static String getRealTimeDateChat(String dateTime) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
        try {
            Date date = sdf.parse(dateTime);

            Calendar messageTime = Calendar.getInstance();
            messageTime.setTime(date);
            Calendar now = Calendar.getInstance();

            final String strDateFormat = "MMM dd, yyyy";

            if (now.get(Calendar.DATE) == messageTime.get(Calendar.DATE)
                    &&
                    ((now.get(Calendar.MONTH) == messageTime.get(Calendar.MONTH)))
                    &&
                    ((now.get(Calendar.YEAR) == messageTime.get(Calendar.YEAR)))
                    ) {

                return "Today, " + String.valueOf(DateFormat.format("hh:mm A", messageTime));

            } else if (
                    ((now.get(Calendar.DATE) - messageTime.get(Calendar.DATE)) == 1)
                            &&
                            ((now.get(Calendar.MONTH) == messageTime.get(Calendar.MONTH)))
                            &&
                            ((now.get(Calendar.YEAR) == messageTime.get(Calendar.YEAR)))
                    ) {
                return "Yesterday, " + DateFormat.format("hh:mm A", messageTime);
            } else {
                return String.valueOf(DateFormat.format(strDateFormat, messageTime) + " " + String.valueOf(DateFormat.format("hh:mm A", messageTime)));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void showSnackbar(View view, String text) {
        if (text.trim().isEmpty() || view == null) return;

        Snackbar snackbar = Snackbar.make(view, text, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    public static void setVectorImage(ImageView imageView, int resourceId) {
        Drawable drawable = create(imageView.getResources(), resourceId, imageView.getContext().getTheme());
        imageView.setImageDrawable(drawable);
    }
}
