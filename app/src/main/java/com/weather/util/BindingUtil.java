package com.weather.util;


import android.widget.ImageView;
import android.widget.TextView;


import com.weather.R;

import androidx.databinding.BindingAdapter;

/**
 * Created by dhaval on 28/11/16.
 */

public class BindingUtil {

    @BindingAdapter({"bind:weather"})
    public static void bind(ImageView view, String weatherID) {
        switch (Integer.parseInt(weatherID)) {
            case 500: {
                Helper.setVectorImage(view, R.drawable.ic_umbrella);
                break;
            }
            case 800: {
                Helper.setVectorImage(view, R.drawable.ic_sunny);

                break;
            }
            default:
                Helper.setVectorImage(view, R.drawable.ic_sun);
        }


    }

    @BindingAdapter({"bind:realDay"})
    public static void bind(TextView view, String dateTime) {
        view.setText(Helper.getRealTimeDateChat(dateTime));
    }

}
