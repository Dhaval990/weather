package com.weather;

public class Constants {
    public static final String METRIC = "METRIC";
    public static final String IMPERIAL = "IMPERIAL";
    public static final String UNIT_SYSTEM = "UNIT_SYSTEM";
}
