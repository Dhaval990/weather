package com.weather.fragment;


import android.app.Fragment;
import android.os.Bundle;

import com.weather.BaseFragment;
import com.weather.R;
import com.weather.databinding.FragmentHelpBinding;

import androidx.annotation.Nullable;

/**
 * A simple {@link Fragment} subclass.
 */
public class HelpFragment extends BaseFragment {
    public static final String TAG = HelpFragment.class.getSimpleName();

    public HelpFragment() {
        // Required empty public constructor
    }

    public static HelpFragment newInstance() {
        HelpFragment fragment = new HelpFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView(R.layout.fragment_help);
    }

    @Override
    public void initVariable() {
        FragmentHelpBinding binding = getBinding();
setToolbarTitle(getString(R.string.help));
        binding.wvHelp.getSettings().setJavaScriptEnabled(true);
        binding.wvHelp.loadDataWithBaseURL("", getString(R.string.help_string), "text/html", "UTF-8", "");

    }

    @Override
    public void loadData() {

    }
}
