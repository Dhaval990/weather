package com.weather.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;


import com.weather.R;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class CustomDialogDataBind extends DialogFragment {
    private String title;
    private String message;
    private String okLbl;
    private String cancelLbl;
    private DialogInterface.OnClickListener okClickListener, cancelClickListener;
    private int layoutId;
    private View view;

    public CustomDialogDataBind() {
        super();
    }


    public void initializeVariable(String title, String message, String okLbl, String cancelLbl, DialogInterface.OnClickListener okListener, DialogInterface.OnClickListener cancelListener, View view) {
        this.title = title;
        this.message = message;
        this.okLbl = okLbl;
        this.cancelLbl = cancelLbl;
        this.okClickListener = okListener;
        this.cancelClickListener = cancelListener;
        //this.layoutId = layoutId;
        this.view = view;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyAlertDialogStyle);
        builder.setView(view);
        //builder.setView(layoutId);
        builder.setTitle(title);
        builder.setMessage(message)
                .setPositiveButton(okLbl, okClickListener)
                .setNegativeButton(cancelLbl, cancelClickListener);
        return builder.create();
    }

}