package com.weather.fragment;

import android.os.Bundle;

import com.weather.BaseFragment;
import com.weather.R;
import com.weather.adapter.WeatherAdapter;
import com.weather.databinding.WeatherFragmentBinding;
import com.weather.models.BookMark;
import com.weather.models.weather.WeatherData;
import com.weather.viewmodel.WeatherViewModel;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class WeatherFragment extends BaseFragment {
    public static final String TAG = WeatherFragment.class.getSimpleName();
    public static final String CITY_DATA = "cityData";

    private WeatherViewModel weatherViewModel;

    private WeatherFragmentBinding binding;
    private WeatherAdapter mWeatherAdapter;


    public static WeatherFragment newInstance(BookMark cityData) {
        WeatherFragment weatherFragment = new WeatherFragment();
        Bundle bundle = new Bundle(1);
        bundle.putParcelable(CITY_DATA, cityData);
        weatherFragment.setArguments(bundle);
        return weatherFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView(R.layout.weather_fragment);
    }

    @Override
    public void initVariable() {
        binding = getBinding();
        setToolbarTitle(getString(R.string.weather_report));

        weatherViewModel = ViewModelProviders.of(this).get(WeatherViewModel.class);

        binding.rcvWeather.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));

    }

    @Override
    public void loadData() {
        weatherViewModel.getWeatherInfo(getContext(), getArguments().getParcelable(CITY_DATA)).observe(this, new Observer<WeatherData>() {
            @Override
            public void onChanged(WeatherData weatherData) {
                if (weatherData != null) {
                    mWeatherAdapter = new WeatherAdapter(weatherData.list);
                    binding.rcvWeather.setAdapter(mWeatherAdapter);
                }
            }
        });
    }
}
