package com.weather.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.weather.BaseFragment;
import com.weather.R;
import com.weather.adapter.ItemAdapter;
import com.weather.databinding.FragmentBookmarkBinding;
import com.weather.interfaces.OnItemClick;
import com.weather.models.BookMark;
import com.weather.module.BookmarkComponent;
import com.weather.module.BookmarkModule;
import com.weather.module.DaggerBookmarkComponent;
import com.weather.util.Helper;
import com.weather.viewmodel.BookMarkViewModel;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookmarkFragment extends BaseFragment implements OnItemClick, View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {
    public static final String TAG = BookmarkFragment.class.getSimpleName();


    private FragmentBookmarkBinding binding;
    private BookmarkComponent daggerBookmarkComponent;
    private BookMarkViewModel bookMarkViewModel;
    private GoogleApiClient mGoogleApiClient;
    private int PLACE_PICKER_REQUEST = 1;


    public BookmarkFragment() {
        // Required empty public constructor
    }

    public static BookmarkFragment newInstance() {
        BookmarkFragment fragment = new BookmarkFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView(R.layout.fragment_bookmark);
    }

    @Override
    public void initVariable() {
        binding = getBinding();
        setToolbarTitle(getString(R.string.saved_city));

        daggerBookmarkComponent = DaggerBookmarkComponent.builder().bookmarkModule(new BookmarkModule(this))
                .build();

        binding.fabAddCity.setOnClickListener(this);

        mGoogleApiClient = new GoogleApiClient
                .Builder(Objects.requireNonNull(getContext()))
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
    }

    @Override
    public void loadData() {
        binding.rcvBookmark.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));

        ItemAdapter adapter = new ItemAdapter(this);

        bookMarkViewModel = daggerBookmarkComponent.inject();

        bookMarkViewModel.getConcertList(getContext()).observe(this, bookMarks -> {
            adapter.submitList(bookMarks);

            if (bookMarks == null || bookMarks.isEmpty()) {
                binding.imgNoData.setVisibility(View.VISIBLE);
            } else {
                binding.imgNoData.setVisibility(View.GONE);
            }
        });
        binding.rcvBookmark.setAdapter(adapter);
    }

    @Override
    public void setOnItemClick(View view, BookMark item, int position) {
        switch (view.getId()) {
            case R.id.card_bookmark_city: {
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frm_main, WeatherFragment.newInstance(item), WeatherFragment.TAG).commit();
                break;
            }
            case R.id.img_bookmark: {
                Helper.showOKCancelDialog(getContext(), "Are you sure you want to remove?", isPressedOK -> {
                    if (isPressedOK) {
                        bookMarkViewModel.removeBookmarkCity(getContext(), item);
                    }
                });
                break;
            }
        }

    }

    @Override
    public void onClick(View v) {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(Objects.requireNonNull(getActivity())), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(getContext(), connectionResult.getErrorMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(Objects.requireNonNull(getContext()), data);
                BookMark bookMark = new BookMark();
                bookMark.cityName = String.format("%s", place.getName());
                bookMark.lat = place.getLatLng().latitude;
                bookMark.log = place.getLatLng().longitude;

                bookMarkViewModel.addBookmarkCity(getContext(), bookMark);
            }
        }
    }
}