package com.weather.fragment;


import android.app.Fragment;
import android.os.Bundle;

import com.weather.BaseFragment;
import com.weather.Constants;
import com.weather.R;
import com.weather.database.AppDatabase;
import com.weather.databinding.FragmentSettingBinding;
import com.weather.util.Helper;
import com.weather.util.PrefManager;

import androidx.annotation.Nullable;

import static com.weather.Constants.UNIT_SYSTEM;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends BaseFragment {


    public SettingFragment() {
        // Required empty public constructor
    }

    public static SettingFragment newInstance() {
        SettingFragment fragment = new SettingFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView(R.layout.fragment_setting);
    }

    @Override
    public void initVariable() {
        FragmentSettingBinding fragmentSettingBinding = getBinding();
        setToolbarTitle(getString(R.string.setting));

        fragmentSettingBinding.txtUnit.setText(PrefManager.getSharedPref(getContext(), UNIT_SYSTEM, Constants.METRIC));

        fragmentSettingBinding.linUnitSystem.setOnClickListener(v -> Helper.showUnitSystemDialog(getContext(), (selectedBookmark, isPressedOK) -> {
            if (isPressedOK) {
                fragmentSettingBinding.txtUnit.setText(selectedBookmark);
            }
        }));

        fragmentSettingBinding.linResetBookmark.setOnClickListener(v -> {
            AppDatabase.getAppDatabase(getContext()).bookmarkDao().deleteAllCity();
            Helper.showSnackbar(v, getString(R.string.bookmark_delete_message));
        });
    }

    @Override
    public void loadData() {

    }
}
