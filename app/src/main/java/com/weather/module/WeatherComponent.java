package com.weather.module;


import com.weather.fragment.WeatherFragment;

import dagger.Component;

@Component(modules = WeatherModule.class)
public interface WeatherComponent {
    void inject(WeatherFragment weatherFragment);
}