package com.weather.module;


import com.weather.activity.MainActivity;
import com.weather.repo.WeatherRepo;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, ApiModule.class})
public interface ApiComponent {
    void inject(MainActivity activity);

    void injectWeatherVM(WeatherRepo weatherRepo);


}