package com.weather.module;


import com.weather.viewmodel.BookMarkViewModel;

import dagger.Component;

@BookmarkScope
@Component(modules = BookmarkModule.class)
public interface BookmarkComponent {

    BookMarkViewModel inject();

}
