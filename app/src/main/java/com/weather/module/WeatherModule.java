package com.weather.module;


import com.weather.fragment.BookmarkFragment;
import com.weather.fragment.WeatherFragment;
import com.weather.viewmodel.WeatherViewModel;

import androidx.lifecycle.ViewModelProviders;
import dagger.Module;
import dagger.Provides;

@Module
public class WeatherModule {

    private WeatherFragment weatherFragment;


    public WeatherModule(WeatherFragment weatherFragment) {
        this.weatherFragment = weatherFragment;
    }

    @Provides
    WeatherViewModel provideWeatherVM(BookmarkFragment bookmarkFragment) {
        return ViewModelProviders.of(bookmarkFragment).get(WeatherViewModel.class);
    }

    @Provides
    WeatherFragment provideWeatherFragment() {
        return weatherFragment;
    }

}
