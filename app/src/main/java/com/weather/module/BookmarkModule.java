package com.weather.module;


import com.weather.fragment.BookmarkFragment;
import com.weather.repo.BookMarkRepo;
import com.weather.viewmodel.BookMarkViewModel;

import androidx.lifecycle.ViewModelProviders;
import dagger.Module;
import dagger.Provides;

@Module
public class BookmarkModule {

    private BookmarkFragment bookmarkFragment;

    public BookmarkModule(BookmarkFragment bookmarkFragment) {
        this.bookmarkFragment = bookmarkFragment;
    }

    @BookmarkScope
    @Provides
    public BookMarkViewModel provideBookmarkVM(BookmarkFragment bookmarkFragment) {
        return ViewModelProviders.of(bookmarkFragment).get(BookMarkViewModel.class);
    }

    @BookmarkScope
    @Provides
    public BookmarkFragment provideBookmarkFragment() {
        return bookmarkFragment;
    }

    @BookmarkScope
    @Provides
    public BookMarkRepo provideBookmarkRepo() {
        return new BookMarkRepo();
    }


}
