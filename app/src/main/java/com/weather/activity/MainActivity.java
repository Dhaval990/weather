package com.weather.activity;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.weather.App;
import com.weather.BaseActivity;
import com.weather.R;
import com.weather.databinding.ActivityMainBinding;
import com.weather.fragment.BookmarkFragment;
import com.weather.fragment.HelpFragment;
import com.weather.fragment.SettingFragment;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import retrofit2.Retrofit;


public class MainActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener {


    @Inject
    Retrofit retrofit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setView(R.layout.activity_main);


    }

    @Override
    public void initVariable() {
        ActivityMainBinding binding = getBinding();
        setToolbar(binding.toolbar);

        ((App) getApplicationContext()).getApiComponent().inject(this);

        binding.bottomNavigation.setOnNavigationItemSelectedListener(this);
    }

    @Override
    public void loadData() {
        getSupportFragmentManager().beginTransaction().replace(R.id.frm_main, BookmarkFragment.newInstance(), BookmarkFragment.TAG).disallowAddToBackStack().commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                if (!(getSupportFragmentManager().findFragmentById(R.id.frm_main) instanceof BookmarkFragment)) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.frm_main, BookmarkFragment.newInstance(), BookmarkFragment.TAG).commit();
                }
                return true;

            case R.id.action_setting:
                if (!(getSupportFragmentManager().findFragmentById(R.id.frm_main) instanceof SettingFragment)) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.frm_main, SettingFragment.newInstance(), SettingFragment.TAG).commit();
                }
                return true;
                case R.id.action_help:
                if (!(getSupportFragmentManager().findFragmentById(R.id.frm_main) instanceof HelpFragment)) {
                    getSupportFragmentManager().beginTransaction().replace(R.id.frm_main, HelpFragment.newInstance(), HelpFragment.TAG).commit();
                }
                return true;

        }
        return false;

    }
}