package com.weather.database;




import com.weather.models.BookMark;

import java.util.List;

import androidx.paging.DataSource;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface BookmarkCityDao {




    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertCity(BookMark bookMark);

    @Delete
    void deleteCity(BookMark bookMark);

    @Query("delete  FROM BookMark")
    void deleteAllCity();

    @Query("SELECT * FROM BookMark ORDER BY cityName")
    DataSource.Factory<Integer, BookMark> bookmarkByName();


}