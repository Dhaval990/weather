package com.weather.interfaces;

import android.view.View;

public interface OnClick {
    void onClick(View view);
}
