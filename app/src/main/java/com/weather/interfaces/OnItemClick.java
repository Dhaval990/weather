package com.weather.interfaces;

import android.view.View;

import com.weather.models.BookMark;


public interface OnItemClick {
    void setOnItemClick(View view, BookMark item, int position);
}
