package com.weather.interfaces;

public interface DialogBookmarkCallback {
    void onDismiss(String selectedBookmark, boolean isPressedOK);
}