package com.weather.interfaces;

import com.weather.models.weather.WeatherData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Dhaval Joshi on 10/08/16.
 */
public interface ApiService {
    String FORECAST = "forecast";


    @GET(FORECAST)
    Call<WeatherData> getCommitsByName(@Query("lat") String lat,
                                       @Query("lon") String lon,
                                       @Query("appid") String appid,
                                       @Query("units") String units);

}