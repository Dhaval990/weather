package com.weather.interfaces;

public interface DialogCallback {
    void onDismiss(boolean isPressedOK);
}