package com.weather.repo;

import android.util.Log;

import com.weather.App;
import com.weather.interfaces.ApiService;
import com.weather.models.BookMark;
import com.weather.models.weather.WeatherData;

import javax.inject.Inject;

import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class WeatherRepo {

    @Inject
    Retrofit retrofit;

    public WeatherRepo() {
        App.getApiComponent().injectWeatherVM(this);

    }

    public void getWeatherData(MutableLiveData<WeatherData> weatherList, BookMark cityData, String unitType) {
        Call<WeatherData> call = retrofit.create(ApiService.class).getCommitsByName(String.valueOf(cityData.lat), String.valueOf(cityData.log), "c6e381d8c7ff98f0fee43775817cf6ad", unitType);
        call.enqueue(new Callback<WeatherData>() {
            @Override
            public void onResponse(Call<WeatherData> call, Response<WeatherData> response) {
                if (response.isSuccessful()) {
                    weatherList.postValue(response.body());
                }
            }

            @Override
            public void onFailure(Call<WeatherData> call, Throwable t) {
                Log.d("onFailure", "onFailure: " + t.getLocalizedMessage());
            }
        });
    }
}
