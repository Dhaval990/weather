package com.weather.repo;


import com.weather.models.BookMark;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.paging.PagedList;

public class BookMarkRepo {
    @Inject
    LiveData<PagedList<BookMark>> pagedListLiveData;

    @Inject
    public BookMarkRepo() {

    }

    public LiveData<PagedList<BookMark>> getPagedListLiveData() {
        return pagedListLiveData;
    }
}