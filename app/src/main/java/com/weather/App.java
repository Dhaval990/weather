package com.weather;


import android.app.Application;

import com.weather.module.ApiComponent;
import com.weather.module.ApiModule;
import com.weather.module.AppModule;
import com.weather.module.DaggerApiComponent;

public class App extends Application {

    private static ApiComponent apiComponent;

    public static ApiComponent getApiComponent() {
        return apiComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        apiComponent = DaggerApiComponent.builder()
                .apiModule(new ApiModule("http://api.openweathermap.org/data/2.5/"))
                .appModule(new AppModule(this)).build();
    }
}
