package com.weather.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.weather.R;
import com.weather.databinding.RawCityBookmarkBinding;
import com.weather.interfaces.OnItemClick;
import com.weather.models.BookMark;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;


public class ItemAdapter extends PagedListAdapter<BookMark, ItemAdapter.ItemViewHolder> {

    private static DiffUtil.ItemCallback<BookMark> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<BookMark>() {
                @Override
                public boolean areItemsTheSame(BookMark oldItem, BookMark newItem) {
                    return oldItem.cityID == newItem.cityID;
                }

                @Override
                public boolean areContentsTheSame(BookMark oldItem, BookMark newItem) {
                    return oldItem.equals(newItem);
                }
            };
    private OnItemClick mOnItemClick;

    public ItemAdapter(OnItemClick onItemClick) {
        super(DIFF_CALLBACK);
        this.mOnItemClick = onItemClick;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_city_bookmark, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        BookMark item = getItem(position);

        if (item != null) {
            holder.binding.setBookmark(item);
            holder.binding.executePendingBindings();
        } else {
            Toast.makeText(holder.binding.getRoot().getContext(), "Item is null", Toast.LENGTH_LONG).show();
        }
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        RawCityBookmarkBinding binding;


        ItemViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);

            binding.imgBookmark.setOnClickListener(v -> mOnItemClick.setOnItemClick(v, getItem(getAdapterPosition()), getAdapterPosition()));
            binding.cardBookmarkCity.setOnClickListener(v -> mOnItemClick.setOnItemClick(v, getItem(getAdapterPosition()), getAdapterPosition()));
        }
    }
}