package com.weather.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.weather.R;
import com.weather.databinding.RawWeatherBinding;
import com.weather.models.weather.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.WeatherViewHolder> {

    private java.util.List<List> listList;

    public WeatherAdapter(java.util.List<List> listList) {
        this.listList = listList;
    }

    @NonNull
    @Override
    public WeatherViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_weather, parent, false);
        return new WeatherViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WeatherViewHolder holder, int position) {
        List item = listList.get(position);
        holder.binding.setList(item);
        holder.binding.executePendingBindings();
      /*  if (item != null) {
            holder.binding.setBookmark(item);

        } else {
            Toast.makeText(holder.binding.getRoot().getContext(), "Item is null", Toast.LENGTH_LONG).show();
        }*/
    }

    @Override
    public int getItemCount() {
        return listList == null ? 0 : listList.size();
    }

    class WeatherViewHolder extends RecyclerView.ViewHolder {

        RawWeatherBinding binding;

        WeatherViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);

            /*binding.cardBookmarkCity.setOnClickListener(v -> mOnItemClick.setOnItemClick(v, getItem(getAdapterPosition()), getAdapterPosition()));
            binding.imgBookmark.setOnClickListener(v -> mOnItemClick.setOnItemClick(v, getItem(getAdapterPosition()), getAdapterPosition()));*/
        }
    }
}