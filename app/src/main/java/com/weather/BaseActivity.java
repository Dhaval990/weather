package com.weather;

import android.graphics.drawable.Drawable;
import android.view.View;

import com.weather.interfaces.ActImpMethods;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;

public abstract class BaseActivity extends AppCompatActivity implements ActImpMethods {

    public static final String TAG = BaseActivity.class.getSimpleName();
    protected Toolbar mToolbar;
    protected ViewDataBinding binding;


    protected void setView(int layoutResId) {
        binding = DataBindingUtil.setContentView(this, layoutResId);
        try {
            initVariable();
            loadData();
        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    protected <T extends ViewDataBinding> T getBinding() {
        return (T) binding;
    }

    protected void setToolbarcolor(int toolbarcolor) {
        mToolbar.setBackgroundColor(toolbarcolor);
    }

    protected void setToolbarDrawable(Drawable toolbarDrawable) {
        mToolbar.setNavigationIcon(toolbarDrawable);
    }

    /**
     * @param toolbar
     * @param title
     * @param isBackEnabled
     */
    protected void setToolbar(Toolbar toolbar, String title, boolean isBackEnabled) {
        this.mToolbar = toolbar;
        super.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setTitle(title);

        if (isBackEnabled) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }

    protected void setToolbar(Toolbar toolbar) {
        this.mToolbar = toolbar;
        super.setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


    }

    public void setToolbarTextColor(int color) {
        mToolbar.setTitleTextColor(color);
    }

    public void setToolbarSubTitleTextColor(int color) {
        mToolbar.setSubtitleTextColor(color);
    }


    public void setToolBarTitle(String title) {
        mToolbar.setTitle(title);
    }


}
