package com.weather.viewmodel;

import android.content.Context;

import com.weather.Constants;
import com.weather.models.BookMark;
import com.weather.models.weather.WeatherData;
import com.weather.repo.WeatherRepo;
import com.weather.util.PrefManager;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import retrofit2.Retrofit;

public class WeatherViewModel extends ViewModel {

    @Inject
    Retrofit retrofit;

    private MutableLiveData<WeatherData> weatherList;


    public LiveData<WeatherData> getWeatherInfo(Context context, BookMark cityData) {
        if (weatherList == null) {
            weatherList = new MutableLiveData<>();
        }
        new WeatherRepo().getWeatherData(weatherList, cityData, PrefManager.getSharedPref(context, Constants.UNIT_SYSTEM, Constants.METRIC));

        return weatherList;

    }

}
