package com.weather.viewmodel;

import android.content.Context;

import com.weather.database.AppDatabase;
import com.weather.models.BookMark;
import com.weather.repo.BookMarkRepo;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;


public class BookMarkViewModel extends ViewModel {


    private BookMarkRepo bookMarkRepo;
    private LiveData<PagedList<BookMark>> concertList;


    public LiveData<PagedList<BookMark>> getConcertList(Context context) {

        if (concertList == null) {
            concertList = new LivePagedListBuilder<>(
                    AppDatabase.getAppDatabase(context).bookmarkDao().bookmarkByName(), 20).build();
        }
        return concertList;
    }

    public void removeBookmarkCity(Context context, BookMark bookMark) {
        AppDatabase.getAppDatabase(context).bookmarkDao().deleteCity(bookMark);
    }

    public void addBookmarkCity(Context context, BookMark bookMark) {
        AppDatabase.getAppDatabase(context).bookmarkDao().insertCity(bookMark);
    }


}
