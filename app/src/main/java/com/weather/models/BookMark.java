package com.weather.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class BookMark implements Parcelable {

    public static final Parcelable.Creator<BookMark> CREATOR = new Parcelable.Creator<BookMark>() {
        @Override
        public BookMark createFromParcel(Parcel source) {
            return new BookMark(source);
        }

        @Override
        public BookMark[] newArray(int size) {
            return new BookMark[size];
        }
    };
    @NonNull
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String cityName;
    public String cityID;
    public double lat;
    public double log;

    public BookMark() {
    }

    protected BookMark(Parcel in) {
        this.id = in.readInt();
        this.cityName = in.readString();
        this.cityID = in.readString();
        this.lat = in.readDouble();
        this.log = in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.cityName);
        dest.writeString(this.cityID);
        dest.writeDouble(this.lat);
        dest.writeDouble(this.log);
    }
}
