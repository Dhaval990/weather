package com.weather.models.weather;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Wind {

    @SerializedName("speed")
    @Expose
    public String speed;
    @SerializedName("deg")
    @Expose
    public String deg;

}
