package com.weather.models.weather;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Main {

    @SerializedName("temp")
    @Expose
    public String temp;
    @SerializedName("temp_min")
    @Expose
    public String tempMin;
    @SerializedName("temp_max")
    @Expose
    public String tempMax;
    @SerializedName("pressure")
    @Expose
    public String pressure;
    @SerializedName("sea_level")
    @Expose
    public String seaLevel;
    @SerializedName("grnd_level")
    @Expose
    public String grndLevel;
    @SerializedName("humidity")
    @Expose
    public String humidity;
    @SerializedName("temp_kf")
    @Expose
    public String tempKf;

}
